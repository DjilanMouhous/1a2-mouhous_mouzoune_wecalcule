
-------------------------- Projet calculette TVA -----------------------------------------------------------                        

Consignes :
- Créer une calculette dont vous avez besoin en tant que Freelance.
- Format iPhone X / iPad 
- Design : export des planches (design) en .PNG
- Développement : uniquement intégration en HTML / CSS (JavaScript / Back à partir du mois de Janvier)
- Features : opérations de base + TVA (20%)
- Rendu attendu : projet sur Gitlab / Gitlab pages
- Utilisation obligatoire de mon starter Parcel https://github.com/davidvenin/parcel-starter-scss
- Présentation

--------------------------------------------------------------------------------------------------------------

Voici donc notre projet réalisé en duo : Mouhous Djilan et Mouzoune Abderrahmane.

Dans notre projet il y a :

- maquette : contient les maquette en PNG et en XD des formats iphone X et ipad 
- code_TVA : contient l'intégration en html scss de nos maquettes
- presentation : contient notre support de présentation en PDF

----------------------------------------------------------------------------------------------------------------